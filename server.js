const http = require('http')
var url = require('url');

const port = 3000

const requestHandler = (request, response) => {
    console.log(request.url);
    var q = url.parse(request.url, true);
    var name = q.query['name'];
    response.writeHead(200, {"Content-Type": "text/html; charset=utf-8"});
    response.end(`Halo ${name}, tu Serwer Node.js!`);
}

const server = http.createServer(requestHandler).listen(8000);