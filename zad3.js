var lineReader = require('readline').createInterface({
    input: require('fs').createReadStream('./suma.txt')
});

var sum = 0;

lineReader.on('line', (line) => {

    sum += parseInt(line);

});

lineReader.on('close', () => console.log(sum))
