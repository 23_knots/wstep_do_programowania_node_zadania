var events = require('events');
var eventEmitter = new events.EventEmitter();
eventEmitter.on('mojeZdarzenie', () => {
    var sieve = [], i, j, primes = [];
    for (i = 2; i <= 20; ++i) {
        if (!sieve[i]) {
            primes.push(i);
            for (j = i << 1; j <= 20; j += i) {
                sieve[j] = true;
            }
        }
    }
    console.log(primes);
});
eventEmitter.emit('mojeZdarzenie');
