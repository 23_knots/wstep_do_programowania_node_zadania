const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question('Podaj liczbę która chcesz podnieść do kwadratu: ', (number) => {
    console.log(`Kwadratem liczby ${number} jest ${number*number}`);
    rl.close();
});